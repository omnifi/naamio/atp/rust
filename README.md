# Naamio AT Protocol in Rust

An implementation of the AT protocol written in Rust.

## Formatting and linting

```shell
cargo fmt
cargo clippy -- -W clippy::pedantic -D warnings
```